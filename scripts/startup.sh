
#!/bin/sh
python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate

# Defina as variáveis para o superusuário
SUPERUSER_USERNAME="analista"
SUPERUSER_EMAIL="analista@example.com"
SUPERUSER_PASSWORD="analista"
SUPERUSER_FIRSTNAME="Analista"

# Use o comando manage.py shell para criar o superusuário
python manage.py shell << END
from django.contrib.auth import get_user_model
User = get_user_model()

if not User.objects.filter(username='$SUPERUSER_USERNAME').exists():
    User.objects.create_superuser('$SUPERUSER_USERNAME', '$SUPERUSER_EMAIL', '$SUPERUSER_PASSWORD', '$SUPERUSER_FIRSTNAME')
    print('Superusuário criado com sucesso.')
else:
    print('Superusuário já existe.')
END

#python config.py
gunicorn --bind 0.0.0.0:8000 --workers 2 laudos.wsgi
tail -f /dev/null