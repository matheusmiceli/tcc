FROM python:latest

RUN apt-get update  -y

RUN apt-get install -y tzdata
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get remove -y tzdata

RUN apt-get install -y build-essential
RUN apt-get install -y libsasl2-dev python3-dev libldap2-dev libssl-dev 
RUN apt-get install -y python-is-python3 python3-pip python3-django

WORKDIR  /opt/sistema_laudos

COPY . .

RUN pip --no-cache install -r requirements.txt

EXPOSE 8000

ENTRYPOINT ["sh", "./scripts/startup.sh"]