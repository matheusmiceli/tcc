from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import User

#Model Dados_laudo: Nesta model é guardado informações sobre o local de onde vem o laudo
class Unidade(models.Model):
    nome = models.CharField(max_length=128)

    def __str__(self):
        return self.nome 
    

class Instituto(models.Model):
    nome = models.CharField(max_length=128)
    
    def __str__(self):
        return f'{self.nome}'


#Model centro_custo: Tabela para guardar dados sobre a relação do centro de custo
class RelacaoCentroCusto(models.Model):
    cod_centro_custo = models.CharField(max_length=128, blank=True)
    unid_negocio = models.CharField(max_length=128)
    nome_centro_custo = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.nome_centro_custo}: {self.cod_centro_custo}.{self.unid_negocio}'
    
    class Meta:
        verbose_name = "Relação de Centros de Custo"
        verbose_name_plural = "Relação de Centros de Custo"


class UserProfile(models.Model):
    unidade = models.ForeignKey(Unidade, on_delete=models.CASCADE, null=True, blank=True)
    primeiro_acesso = models.BooleanField(default=True)
    user = models.OneToOneField(
        User, on_delete=models.CASCADE
    )

    def __str__(self):
        return f'O usuario {self.user} esta em seu primeiro acesso'

# Model Ques cria a tabela Laudos no banco de dados, nela existe a diferenciação entre laudo de manutenção ou
# devolução
class Laudos(models.Model):

    tipos = (
        ('', 'Selectione o tipo de equipamento'),
        ('Computador', 'Computador'),
        ('Monitor', 'Monitor'),
        ('Notebook', 'Notebook'),
        ('Switch', 'Switch'),
        ('Projetor', 'Projetor'),
        ('Impressora', 'Impressora')
    )

    status = (
        ('em-progresso', 'Em Progresso'), 
        ('Devolvido-ao-patrimonio', 'Devolvido ao Patrimônio'),
        ('Aguardando-resposta', 'Aguardado Resposta'),
        ('Concluido', 'Concluído')
    )

    tipo_laudo = (
        ('manutencao', 'Manutenção'),
        ('devolucao', 'Devolução')
    )

    baixas = (
        ('', 'Selecione o motivo de devolução'),
        ('sucateamento', 'Baixa por Sucateamento'),
        ('baixo desempenho', 'Baixa por Desempenho')
    )

    nome_solicitante = models.CharField(max_length=128)
    email_solicitante = models.EmailField()
    num_chamado = models.CharField(max_length=14, blank=True, null=True)
    num_sala = models.CharField(max_length=20, blank=True, null=True)
    num_predio = models.IntegerField()
    num_ramal = models.IntegerField()
    setor = models.CharField(max_length=128)
    tipo_equip = models.CharField(max_length=128, choices=tipos)
    num_serie = models.CharField(max_length=128)
    patrimonio = models.CharField(max_length=14, blank=True, null=True)
    marca = models.CharField(max_length=55)
    modelo = models.CharField(max_length=55)
    observacoes = models.TextField()
    status = models.CharField(max_length=128, choices=status)
    tipo_laudo = models.CharField(max_length=128, choices=tipo_laudo)
    tipo_baixa = models.CharField(max_length=128, choices=baixas, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    unidade = models.ForeignKey(Unidade, on_delete=models.RESTRICT)
    instituto = models.ForeignKey(Instituto, on_delete=models.RESTRICT)
    centro_custo = models.ForeignKey(RelacaoCentroCusto, on_delete=models.RESTRICT, null=True, blank=True)
    criacao = models.DateTimeField(auto_now_add=True)
    

    class Meta:
        verbose_name = 'Laudos'
        verbose_name_plural = 'Laudos'


    def __str__(self):
        return f'Laudo do tipo {self.tipo_laudo} com patrimônio - {self.patrimonio} e número de serie - {self.num_serie} foi criado {self.criacao}'