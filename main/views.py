from typing import Any, Dict
from django.contrib import messages
from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.generic import TemplateView, View,ListView
from .models import *
from .forms import FormLaudoDevolucao, FormLaudoManutencao, SignUpForm
import json as simplejson
from .serializers import *
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q
from datetime import datetime


# View onde o usuario ve o datatables onde tem a informação de todos os laudos, é uma api onde se limita quantos laudos serão enviados ao 
# front-end, tem uma função de filtragem e envia dados do banco em formato de JSON
class ApiLaudoMain(LoginRequiredMixin, View):
    

    #Função que conta quantos laudos existem no banco
    def numero_elementos(self, request):
        laudos = Laudos.objects.filter(unidade=request.user.userprofile.unidade).count()
        return laudos
    

    #Função de envio para visualização dos dados    
    def get(self, request):

        
        #Variaveis pegas do front-end especificamente do datatables
        inicio = int(request.GET.get('inicio'))
        fim = int(request.GET.get('limite'))
        search = request.GET.get('filtro')
        total = 0
    
        user_profile = request.user.userprofile
        

        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        if search:
            filtro = Q(num_chamado__icontains=search) | Q(unidade__nome__icontains=search) | Q(patrimonio__icontains=search) | Q(user__last_name__icontains=search) | Q(status__icontains=search)
            
            laudos = Laudos.objects.filter(unidade=request.user.userprofile.unidade).filter(filtro)[inicio: inicio+fim]
            
            total = Laudos.objects.filter(unidade=request.user.userprofile.unidade).filter(filtro).count()
        
        # Quando não tem pesquisa envia os dados separadamente
        else:
            laudos = Laudos.objects.filter(unidade=request.user.userprofile.unidade).order_by('-criacao')[inicio: inicio+fim]
            total = Laudos.objects.filter(unidade=request.user.userprofile.unidade).count()

        # Serializando para enviar em JSON
        laudos_ser = LaudoSerializer(laudos, many=True)

        # dicionario das informações a serem enviadas
        data = {
            'objects': laudos_ser.data,
            'length': self.numero_elementos(request),
            'total': total
        }

        

        # Envio dessas informações em JSON
        return JsonResponse({'data': data})
        



#View de visualização da pagina html
class pag_main(LoginRequiredMixin, TemplateView):

    def get(self, request):
        usuario_logado = request.user

        user_profile = request.user.userprofile

        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        return render(request, 'main_templates/main_laudos.html', {'usuario': usuario_logado})
    


class ApiLaudosUnidades(LoginRequiredMixin, View):

    def numero_elementos(self, request):
        laudos = Laudos.objects.all().count()
        return laudos

    def get(self, request):

        #Variaveis pegas do front-end especificamente do datatables
        inicio = int(request.GET.get('inicio'))
        fim = int(request.GET.get('limite'))
        search = request.GET.get('filtro')
        total = 0

        user_profile = request.user.userprofile

        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')
        
        if search:
            filtro = Q(num_chamado__icontains=search) | Q(unidade__nome__icontains=search) | Q(patrimonio__icontains=search) | Q(user__last_name__icontains=search) | Q(status__icontains=search)
            
            laudos = Laudos.objects.all().filter(filtro)[inicio: inicio+fim]
            
            total = Laudos.objects.all().filter(filtro).count()
        
        # Quando não tem pesquisa envia os dados separadamente
        else:
            laudos = Laudos.objects.all().order_by('-criacao')[inicio: inicio+fim]
            total = Laudos.objects.all().count()

        # Serializando para enviar em JSON
        laudos_ser = LaudoSerializer(laudos, many=True)

        # dicionario das informações a serem enviadas
        data = {
            'objects': laudos_ser.data,
            'length': self.numero_elementos(request),
            'total': total
        }

        return JsonResponse({'data': data})




class LaudosUnidades(PermissionRequiredMixin, View):

    permission_required = 'auth.ST_LAUDOS_ADD_DEVOLUCAO'

    def get(self, request):

        usuario_logado = request.user

        user_profile = request.user.userprofile

        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        return render(request, 'main_templates/laudos_unidades.html', {'usuario': usuario_logado})




class DevolucaoView(PermissionRequiredMixin, View):
    template_name = 'main_templates/devolucao.html'

    permission_required = 'auth.ST_LAUDOS_ADD_DEVOLUCAO'
    
    #Metodo get que me retorna o formulario na pagina
    def get(self, request):

        user_profile = request.user.userprofile
        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        form = FormLaudoDevolucao()
        form.user = request.user
        return render(request, 'main_templates/devolucao.html', {'form': form})    
    
    def post(self, request):
        form = FormLaudoDevolucao(request.POST)

        if form.is_valid():
            devolucao = form.save(commit=False)
            if not devolucao.num_sala:
                devolucao.num_sala = 0

            if devolucao.setor is None:
                devolucao.departamento = 'Não Vinculado'

            if devolucao.num_chamado is None or devolucao.num_chamado == "0":
                devolucao.num_chamado = 'Interno CRC'

            if devolucao.patrimonio is None or devolucao.patrimonio == "0":
                devolucao.patrimonio = 'Ausente' 

            devolucao.save()
            messages.success(request, 'Laudo de Devolução criado com sucesso')
            return redirect(reverse('main_laudos'))
        

class ManutencaoView(PermissionRequiredMixin, View):

    permission_required = 'auth.ST_LAUDOS_ADD_MANUTENCAO'

    def get(self, request):

        user_profile = request.user.userprofile
        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        form = FormLaudoManutencao()
        form.user = request.user
        return render(request, 'main_templates/manutencao.html', {'form': form})

    def post(self, request):
        form = FormLaudoManutencao(request.POST)
       
        if form.is_valid():
            manutencao = form.save(commit=False)
            if not manutencao.num_sala:
                manutencao.num_sala = 0

            if manutencao.setor is None:
                manutencao.departamento = 'Não Vinculado'

            if manutencao.num_chamado is None or manutencao.num_chamado == "0":
                manutencao.num_chamado = 'Interno CRC'

            if manutencao.patrimonio is None or manutencao.patrimonio == "0":
                manutencao.patrimonio = 'Ausente'     
            manutencao.save()
            messages.success(request, 'Laudo de Manutenção criado com sucesso')
            return redirect(reverse('main_laudos'))
        
    

   
class ApiCentroCusto(PermissionRequiredMixin, View):

    permission_required = 'auth.ST_LAUDOS_ADD_CENTRO_CUSTO'

    def post(self, request):
        nome_cc = request.POST.get('nome_cc')
        cod_cc = request.POST.get('cod_cc')
        unidade_negocio = request.POST.get('unidade_negocio')

        if nome_cc and cod_cc and unidade_negocio:
            cc, created = RelacaoCentroCusto.objects.get_or_create(cod_centro_custo=cod_cc, unid_negocio=unidade_negocio, nome_centro_custo=nome_cc)

            cc = {
                'id': cc.id,
                'cod_centro_custo': cc.cod_centro_custo,
                'unid_negocio': cc.unid_negocio,
                'created': created,
                'nome_centro_custo': cc.nome_centro_custo
            }
            
            return JsonResponse({'res': 'Criado', "cc": simplejson.dumps(cc)})
        
        else:
            return JsonResponse({'res': 'Erro campos criados em branco'}, status=400)
        
    def get(self, request):
        relacao_cc = RelacaoCentroCusto.objects.all()
        serializers = RelacaoCCSerializer(relacao_cc, many=True)

        return JsonResponse({'res': 'criado', "cc": serializers.data})
    
    
class ApiEditCC(PermissionRequiredMixin, View):

    permission_required = 'auth.ST_LAUDOS_EDIT_CENTRO_CUSTO'

    def post(self, request, id):
        try:        

            cc = RelacaoCentroCusto.objects.get(pk=id)
            cc.nome_centro_custo = request.POST.get('nome_cc')
            cc.cod_centro_custo = request.POST.get('cod_cc')
            cc.unid_negocio = request.POST.get('unid_cc')

            cc.save()

            cc_serializer = RelacaoCCSerializer(cc)
            

            return JsonResponse({'res': 'Editado com sucesso', 'cc': cc_serializer.data})
        except:
            return HttpResponse({'res': 'Erro ao editar centro de custo'}, status=400)



class ApiLaudoVisualizacao(LoginRequiredMixin, View):
    def get(self, request, id):
        laudos = Laudos.objects.get(pk=id)
        serializers = LaudoSerializer(laudos)
        
        return JsonResponse({"status": "Sucess", "data": serializers.data})
    

# classe que edita o laudo de manutanção
class EditManutencao(PermissionRequiredMixin, View):

    permission_required = 'auth.ST_LAUDOS_EDIT_LAUDO'

    # Na função get é retornado todos os campos ja preenchidos conforme o id selecionado
    def get(self, request, id):
        
        user_profile = request.user.userprofile
        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        laudo = Laudos.objects.get(pk=id)
        form = FormLaudoManutencao(instance=laudo)
        return render(request, 'main_templates/editmanutencao.html', {'form': form, 'laudo': laudo})

    # Na função post o usuário pode preencher ou editar algum campo e o mesmo sera salvo no banco
    def post(self, request, id):
        laudo = Laudos.objects.get(pk=id)
        form = FormLaudoManutencao(request.POST, instance=laudo)
        user = laudo.user

        
        if form.is_valid():
            if form.has_changed():
                laudo.user = user
                laudo.save()
                messages.success(request, 'O Laudo foi editado com sucesso')
                return redirect(reverse('main_laudos'))
            else:
                messages.info(request, 'Laudo não foi editado')
                return redirect(reverse('main_laudos'))
        else:
            return HttpResponse('Error Editar!')
        
        

# classe que edita o laudo de devolução
class EditDevolucao(PermissionRequiredMixin, View):
    
    permission_required = 'auth.ST_LAUDOS_EDIT_LAUDO'

    # Na função get é retornado todos os campos ja preenchidos conforme o id selecionado
    def get(self, request, id):

        user_profile = request.user.userprofile
        if user_profile.primeiro_acesso == True:
            return redirect('seleciona_unidade')

        laudo = Laudos.objects.get(pk=id)
        form = FormLaudoDevolucao(instance=laudo)
        return render(request, 'main_templates/editdevolucao.html', {'form': form, 'laudo': laudo})
    
    # Na função post o usuário pode preencher ou editar algum campo e o mesmo sera salvo no banco
    def post(self, request, id):
        laudo = Laudos.objects.get(pk=id)
        form = FormLaudoDevolucao(request.POST, instance=laudo)
        user = laudo.user

        
        if form.is_valid():
            
            if form.has_changed():
                laudo.user = user
                laudo.save()
                messages.success(request, 'O Laudo foi editado com sucesso')
                return redirect(reverse('main_laudos'))
            else:
                messages.info(request, 'Laudo não foi editado')
                return redirect(reverse('main_laudos'))
        return HttpResponse('Erro no envio dos dados!!')
    
        

class ApiAlteraStatus(LoginRequiredMixin, View):

    def post(self, request, id):

        laudo = Laudos.objects.get(pk=id)

        if laudo.status == 'em-progresso':
            laudo.status = 'Devolvido-ao-patrimonio'
            laudo.save()

        if laudo.status == 'Aguardando-resposta':
            laudo.status = 'Concluido'
            laudo.save()
           

        return JsonResponse({'res': 'Editado com sucesso'})



class SelecionaUnidade(View):

    def get(self, request):
        unidades = Unidade.objects.all()

        return render(request, 'main_templates/seleciona_unidade.html', {'unidades': unidades})
    
    def post(self, request):
        
        user_profile = request.user.userprofile

        unidade = request.POST.get('unidade')

        instancia_unidade = Unidade.objects.get(id=unidade)

        if user_profile.primeiro_acesso == True:
            
            user_profile.primeiro_acesso = False
            user_profile.unidade = instancia_unidade

            user_profile.save()

            return redirect('main_laudos')

        else:
            return HttpResponse('Erro de Unidade!!')


def sign_up(request):
    if request.method == "POST":
        fm = SignUpForm(request.POST)
        if fm.is_valid():
            fm.save()
            messages.success(request, 'Usuário criado com sucesso!')
            return redirect('main_laudos')
    else:
        fm = SignUpForm()
    return render(request, 'main_templates/cadastro.html', {'form':fm})