from rest_framework import serializers
from .models import *



class RelacaoCCSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = RelacaoCentroCusto
        fields = '__all__'




class LaudoSerializer(serializers.ModelSerializer):

    user = serializers.CharField(source='user.last_name')
    unidade = serializers.CharField(source='unidade.nome')
    instituto = serializers.CharField(source='instituto.nome')
    nome_centro_custo = serializers.CharField(source="centro_custo.nome_centro_custo", default=None)
    unidade_negocio = serializers.CharField(source='centro_custo.unid_negocio',  default=None)
    centro_custo = serializers.CharField(source='centro_custo.cod_centro_custo',  default=None)

    class Meta:
        model = Laudos
        fields = '__all__'
