from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.pag_main.as_view(), name="main_laudos"),
    path("accounts/", include('django.contrib.auth.urls')),
    path("devolucao", views.DevolucaoView.as_view(), name="devolucao"),
    path("manutencao", views.ManutencaoView.as_view(), name="manutencao"),
    path("api-cc/", views.ApiCentroCusto.as_view(), name="api-cc"),
    path("api-laudo-visu/<int:id>/", views.ApiLaudoVisualizacao.as_view(), name='api-laudo-visualizacao'),
    path("edit-man/<int:id>/", views.EditManutencao.as_view(), name="edit-man"),
    path("edit-dev/<int:id>/", views.EditDevolucao.as_view(), name="edit-dev"),
    path("api-edit-cc/<int:id>/", views.ApiEditCC.as_view(), name="edit-cc"),
    path("api-laudo-main", views.ApiLaudoMain.as_view(), name='api-laudo-main'),
    path("api_altera_status/<int:id>/", views.ApiAlteraStatus.as_view(), name='api-status'),
    path("seleciona_unidade", views.SelecionaUnidade.as_view(), name="seleciona_unidade"),
    path("laudos_unidades", views.LaudosUnidades.as_view(), name="laudos_unidades"),
    path("api_laudos_unidades", views.ApiLaudosUnidades.as_view(), name="api_laudos_unidades"),
]
