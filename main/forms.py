from django import forms
from .models import Laudos
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class FormLaudoDevolucao(forms.ModelForm):

    class Meta:
        model = Laudos
        fields = ['nome_solicitante', 'email_solicitante', "unidade", "instituto", "setor",
                  "num_chamado", "num_predio", "num_sala", "num_ramal", "tipo_equip", "status",
                  "num_serie", "patrimonio", "marca", "modelo", "tipo_baixa", "observacoes",
                  "user", "tipo_laudo"]




class FormLaudoManutencao(forms.ModelForm):

    class Meta:
        model = Laudos
        fields = ["nome_solicitante", "email_solicitante", "unidade", "instituto", "setor",
                "num_chamado", "num_predio", "num_sala", "num_ramal", "tipo_equip", "num_serie",
                "patrimonio", "marca", "modelo", "observacoes", "centro_custo", "user", "status",
                "tipo_laudo"
        ]


class SignUpForm(UserCreationForm):
    password1 = forms.CharField(label="Senha", widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2 = forms.CharField(label="Confirme a senha", widget=forms.PasswordInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label="E-mail",widget=forms.TextInput(attrs={'class':'form-control'}), required=True)
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']
        labels = {'email': 'E-mail', 'last_name': 'Sobrenome', 'first_name': 'Nome', 'password1': 'Senha', 'password2': 'Confirme a senha'}
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class':'form-control'}),
            'last_name': forms.TextInput(attrs={'class':'form-control'}),
        }
