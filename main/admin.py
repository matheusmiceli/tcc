from django.contrib import admin
from .models import  Instituto, RelacaoCentroCusto, Unidade, Laudos, UserProfile
from django.contrib.auth.models import User, Group, Permission
# Register your models here.

class CentroCustoAdmin(admin.ModelAdmin):
    list_display = ('cod_centro_custo', 'unid_negocio')

admin.site.register(Permission)
admin.site.register(RelacaoCentroCusto, CentroCustoAdmin)
admin.site.register(Unidade)
admin.site.register(Instituto)
admin.site.register(Laudos)
admin.site.register(UserProfile)
