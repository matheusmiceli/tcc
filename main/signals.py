from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from .models import UserProfile


@receiver(user_logged_in)

def criando_user_profile(sender, user, request, **kwargs):

    usuario, created = UserProfile.objects.get_or_create(user=user)
